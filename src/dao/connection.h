#ifndef CONNECTION_H
#define CONNECTION_H

#include <QObject>

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QStandardItemModel>
#include <QMessageBox>
#include <QDebug>

class Connection : public QObject
{
    Q_OBJECT
public:
    explicit Connection(QObject *parent = 0);
    ~Connection();

    bool createTable(QString a_tableName, QList<QPair<QString, QString> > a_listCamposTipo);
    bool insertTable(QString a_tableName, QStringList a_listValues);
    bool deleteRow(QString a_tableName);
    QStandardItemModel *selectAll(QString a_tableName, QStandardItemModel * a_model = NULL);

//Static functions
    static bool createConnection()
    {
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName("ComposeList.pdb");
        if (!db.open()) {
            QMessageBox::critical(0
                                , tr("Cannot open database")
                                , tr("Unable to establish a database connection.\n"
                                     "This example needs SQLite support. Please read "
                                     "the Qt SQL driver documentation for information how "
                                     "to build it.\n\n"
                                     "Click Cancel to exit.")
                                , QMessageBox::Cancel);
            return false;
        }

        return true;
    }

    static QSqlDatabase* instanceDB();
    static void release();

private:
    static QSqlDatabase * m_db;

signals:
    void signal_criarLista();

public slots:
    bool slot_deleteRow(QString a_tableName, QString a_codVendedor);
    bool slot_update(QString a_tableName, uint a_order, uint a_status, QString a_codVendedor);
};

#endif // CONNECTION_H
