#include "connection.h"
#include <QSqlError>

QSqlDatabase * Connection::m_db = NULL;
Connection::Connection(QObject *parent) : QObject(parent)
{

}

Connection::~Connection()
{

}

bool Connection::createTable(QString a_tableName, QList<QPair<QString, QString> > a_listCamposTipo)
{
    QString sql;
    QString endSQL;
    QString token;
    QSqlQuery query(*m_db);
    QPair<QString, QString> values;
    bool ok = false;

    ok = query.exec(QString(" DROP TABLE %1; ").arg(a_tableName));

    sql = QString(" CREATE TABLE %1 ( ").arg(a_tableName);
    token = QString(" , ");
    endSQL = QString( " );" );

    int count = 1;
    if (!a_listCamposTipo.isEmpty()) {

        foreach (values, a_listCamposTipo) {
            sql = sql + values.first + " " + values.second;

            if (count < a_listCamposTipo.size()) {
                sql = sql + token;
            }

            count++;
        }
    }

    sql = sql + endSQL;

    ok = query.exec(sql);

    return ok;
}

bool Connection::insertTable(QString a_tableName, QStringList a_listValues)
{
    QString sql;
    QString endSQL;
    QString token;
    QSqlQuery query(*m_db);
    QString value;
    bool ok;

    sql = QString(" INSERT OR REPLACE INTO %1 VALUES ( ").arg(a_tableName);
    token = QString(" , ");
    endSQL = QString( " );" );

    int count = 1;
    if (!a_listValues.isEmpty()) {
        foreach (value, a_listValues) {
            sql = sql + QString("'%1'").arg(value);

            if (count < a_listValues.size()) {
                sql = sql + token;
            }

            count++;
        }
    }

    sql = sql + endSQL;

    ok = query.exec(sql);

    return ok;
}

bool Connection::deleteRow(QString a_tableName)
{
    Q_UNUSED(a_tableName)
    return true;
}

QStandardItemModel *Connection::selectAll(QString a_tableName, QStandardItemModel *a_model)
{
    QSqlQuery query(*m_db);
    QString sql = QString(" SELECT * FROM '%1'; ").arg(a_tableName);

    if (query.exec(sql)) {
        a_model->clear();

         while(query.next()) {
            a_model->appendRow(new QStandardItem(query.value(0).toString()
                                               + " - "
                                               + query.value(1).toString()));
         }
    }

    return a_model;
}

QSqlDatabase *Connection::instanceDB()
{
    if (!m_db)
    {
        createConnection();
    }

    return m_db;
}

void Connection::release()
{
    if (m_db)
    {
        m_db->close();
        delete m_db;
        m_db = NULL;
    }
}

bool Connection::slot_deleteRow(QString a_tableName, QString a_codVendedor)
{
    QSqlQuery query(*m_db);

    QString sql = QString(" DELETE FROM '%1' WHERE u_codvendedor = '%2'; ")
                         .arg(a_tableName)
                         .arg(a_codVendedor);

    return query.exec(sql);

}

bool Connection::slot_update(QString a_tableName, uint a_order, uint a_status, QString a_codVendedor)
{
    QSqlQuery query(*m_db);

    QString sql = QString(" UPDATE '%1' "
                          "    SET u_order = '%2' "
                          "    AND u_status = '%3' "
                          "  WHERE u_codvendedor = '%4' ")
                         .arg(a_tableName)
                         .arg(a_order)
                         .arg(a_status)
                         .arg(a_codVendedor);

    return query.exec(sql);
}

