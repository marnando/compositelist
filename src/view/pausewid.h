#ifndef PAUSEWID_H
#define PAUSEWID_H

#include <QWidget>
#include <QCloseEvent>

namespace Ui {
class PauseWid;
}

class PauseWid : public QWidget
{
    Q_OBJECT

public:
    explicit PauseWid(QWidget *parent = 0);
    ~PauseWid();

    void closeEvent(QCloseEvent * a_event);

private slots:
    void on_m_pbClose_clicked();
    void on_m_lvPausas_clicked(const QModelIndex &index);

private:
    Ui::PauseWid *ui;
};

#endif // PAUSEWID_H
