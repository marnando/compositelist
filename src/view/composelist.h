#ifndef COMPOSELIST_H
#define COMPOSELIST_H

#include <QWidget>

namespace Ui {
class ComposeList;
}

class ComposeList : public QWidget
{
    Q_OBJECT

public:
    explicit ComposeList(QWidget *parent = 0);
    ~ComposeList();

private slots:

    void on_m_lvComposeList_clicked(const QModelIndex &index);

private:
    Ui::ComposeList *ui;
};

#endif // COMPOSELIST_H
