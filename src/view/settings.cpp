#include "settings.h"
#include "ui_settings.h"

Settings::Settings(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
}

Settings::~Settings()
{
    delete ui;
}

void Settings::on_m_gbRoot_clicked(bool a_checked)
{
    Q_UNUSED(a_checked)
}

void Settings::on_m_cbIdioma_currentIndexChanged(const QString &a_arg1)
{
    Q_UNUSED(a_arg1)
}

void Settings::on_m_leMaticula_editingFinished()
{

}
