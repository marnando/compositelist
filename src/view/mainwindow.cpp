#include <QDesktopWidget>
#include <QMessageBox>
#include "mainwindow.h"
#include "composelist.h"
#include "statics.h"
#include "settings.h"
#include "atendance.h"
#include "pausewid.h"
#include "addwid.h"
#include "removewid.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_composeList(new ComposeList(this)),
    m_statics(new Statics(this)),
    m_settings(new Settings(this)),
    m_atendance(0),
    m_pauseWid(new PauseWid(this)),
    m_addWid(new AddWid(this)),
    m_removeWid(new RemoveWid(this)),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initUi();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initUi()
{
    m_orientation = getOrientation();

    m_composeList->hide();
    m_statics->hide();
    m_settings->hide();

    //Modal UI's
    m_pauseWid->hide();
    m_addWid->hide();
    m_removeWid->hide();

    ui->m_mainDockLayout->addWidget(m_composeList);
    ui->m_mainDockLayout->addWidget(m_statics);
    ui->m_mainDockLayout->addWidget(m_settings);

    onShowComposeList();
}

void MainWindow::centralizeMyWidget(QWidget *a_miMod, double a_spacer, double a_percentMaxHeigth, double a_percentMaxWidth)
{
    //Setando haltura e largura geométrica máxima para elementos modais
    if (a_percentMaxHeigth > 0
    &&  a_percentMaxWidth > 0
    &&  a_miMod->parentWidget()) {
        a_miMod->setMaximumHeight(a_miMod->parentWidget()->height() * a_percentMaxHeigth);
        a_miMod->setMaximumWidth(a_miMod->parentWidget()->height() * a_percentMaxWidth);
    }

    //Setando as flags modais e definindo o novo tamanho para widget
    a_miMod->setWindowFlags(Qt::Dialog);
    a_miMod->setWindowModality(Qt::ApplicationModal);
    QDesktopWidget* l_desk = QApplication::desktop();
    QSize l_newsize = QSize (a_miMod->parentWidget()->width()/a_spacer, a_miMod->parentWidget()->height()/a_spacer);

    a_miMod->setVisible(true);
    a_miMod->raise();
    a_miMod->resize(l_newsize);
    a_miMod->updateGeometry();

    //Centralizando
    QSize l_size = a_miMod->size();
    int w = l_desk->width();
    int h = l_desk->height();
    int mw = l_size.width();
    int mh = l_size.height();
    int cw = (w/2) - (mw/2);
    int ch = (h/2) - (mh/2);

    a_miMod->move(cw,ch);

    QApplication::setActiveWindow(a_miMod);
    a_miMod->setFocus();
}

ComposeList *MainWindow::getComposeListWid()
{
    return m_composeList;
}

Statics *MainWindow::getStaticsWid()
{
    return m_statics;
}

Settings *MainWindow::getSettingesWid()
{
    return m_settings;
}

Atendance *MainWindow::getAtendanceWid()
{
    return m_atendance;
}

AddWid *MainWindow::getAddWid()
{
    return m_addWid;
}

PauseWid *MainWindow::getPauseWid()
{
    return m_pauseWid;
}

RemoveWid *MainWindow::getRemoveWid()
{
    return m_removeWid;
}

void MainWindow::resizeEvent(QResizeEvent *a_event)
{
    QSize size = a_event->size();
    bool isLandScape = size.width() > size.height();

    if (isLandScape
    &&  m_orientation != Qt::Horizontal) {
        m_orientation = Qt::Horizontal;

    } else {
        m_orientation = Qt::Vertical;
    }

    QWidget * wid = qApp->activeWindow();
    if (wid && wid->isModal())
    {

        QRect scr = QApplication::desktop()->screenGeometry();
        wid->move( scr.center() - rect().center() );

//        wid->move( window()->frameGeometry().topLeft()
//                + (window()->rect().center() - rect().center()) );

//        if (m_orientation == Qt::Vertical)
//        {
//            centralizeMyWidget(modalWid, 1.1);
//        } else
//        {
//            centralizeMyWidget(modalWid, 1.2);
//        }

//        QWidget::resizeEvent(a_event);

    } else
    {
        a_event->ignore();
    }

}

void MainWindow::closeEvent(QCloseEvent *a_event)
{
    if (m_rootWidObjectName == "ComposeList")
    {
        if (QMessageBox::information(this
                                   , tr("Atenção")
                                   , tr("Deseja realmente sair?")
                                   , QMessageBox::Yes
                                   , QMessageBox::No)
        ==  QMessageBox::No)
        {
            a_event->ignore();
            return;
        }

    } else
    {
        //Ignoro por não ser a minha lista principal
        a_event->ignore();

        //Chamo a lista principal quando aperto o close ou back button
        onShowComposeList();
        return;
    }

    QWidget::closeEvent(a_event);
}

Qt::Orientation MainWindow::getOrientation()
{
    if (QApplication::desktop()->width() > QApplication::desktop()->height())
    {
        return Qt::Horizontal;
    } else
    {
        return Qt::Vertical;
    }
}

void MainWindow::messageIntInfo(QString a_text, QList<int> a_vars)
{
    QString message = a_text;
    foreach (int value, a_vars) {
        message.arg(value);
    }

    QMessageBox::information(this
                           , tr("Atenção")
                           , message
                           , QMessageBox::Ok);
}

void MainWindow::onShowComposeList()
{
    m_statics->hide();
    m_settings->hide();
    m_composeList->show();

    m_rootWidObjectName = m_composeList->objectName();

    ui->m_lblTitulo->setText(tr("Vendedores Ativos"));
}

void MainWindow::onShowStaticsList()
{
    m_composeList->hide();
    m_settings->hide();
    m_statics->show();

    m_rootWidObjectName = m_statics->objectName();

    ui->m_lblTitulo->setText(tr("Estatísticas"));
}

void MainWindow::onShowSettings()
{
    m_composeList->hide();
    m_statics->hide();
    m_settings->show();

    m_rootWidObjectName = m_settings->objectName();

    ui->m_lblTitulo->setText(tr("Configurações"));
}

void MainWindow::on_m_pbAdd_clicked()
{

//    m_addWid->setMinimumHeight(this->height() * 0.70);

//    m_addWid->setMaximumWidth(this->width() * 0.70);
//    m_addWid->setMinimumWidth(this->width() * 0.80);

    if (m_orientation == Qt::Vertical)
    {
        m_addWid->setMaximumHeight(this->height() * 0.80);
//        centralizeMyWidget(m_addWid, 1.2);
    } else
    {
        m_addWid->setMaximumHeight(this->height() * 0.90);
//        centralizeMyWidget(m_addWid, 1.3);
    }

    QRect scr = QApplication::desktop()->screenGeometry();
    QPoint scrCenter = scr.center();
    QPoint rectCenter = rect().center();
    QPoint move =  scr.center() - rect().center();


//    messageIntInfo(QString("Scr center x = %1 e y = %2 \nRect center x = %3 e y = %4\nMove x = %5 e y = %6")
//                    , QList() << scrCenter.x() << scrCenter.y() << rectCenter.x() << rectCenter.y() << move.x() << move.y());

    QMessageBox::information(this
                           , tr("Atenção")
                           , tr("Scr center x = %1 e y = %2 \nRect center x = %3 e y = %4\nMove x = %5 e y = %6" )
                             .arg(scrCenter.x())
                             .arg(scrCenter.y())
                             .arg(rectCenter.x())
                             .arg(rectCenter.y())
                             .arg(move.x())
                             .arg(move.y())
                           , QMessageBox::Ok);

    QMessageBox::information(this
                           , tr("Atenção")
                           , tr("Width = %1 e Height = %2" )
                             .arg(width())
                             .arg(height())
                           , QMessageBox::Ok);

//    m_addWid->move( scr.center() - rect().center() );
    m_addWid->move( width()/2, height()/2 );

//    m_addWid->move( window()->frameGeometry().topLeft()
//                 + window()->rect().center() - rect().center() );

    m_addWid->setWindowFlags(Qt::Dialog);
    m_addWid->setWindowModality(Qt::ApplicationModal);
    m_addWid->show();
    m_addWid->raise();
    qApp->setActiveWindow(m_addWid);
    m_addWid->setFocus();
}

void MainWindow::on_m_pbRemove_clicked()
{
    centralizeMyWidget(m_removeWid, 1.3);
}

void MainWindow::on_m_pbPause_clicked()
{
    centralizeMyWidget(m_pauseWid, 1.3);
}

void MainWindow::on_m_pbStatistic_clicked()
{
    onShowStaticsList();
}

void MainWindow::on_m_pbBack_clicked()
{
    onShowComposeList();
}

void MainWindow::on_m_pbSettings_clicked()
{
    onShowSettings();
}
