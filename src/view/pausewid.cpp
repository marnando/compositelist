#include "pausewid.h"
#include "ui_pausewid.h"

PauseWid::PauseWid(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PauseWid)
{
    ui->setupUi(this);
}

PauseWid::~PauseWid()
{
    delete ui;
}

void PauseWid::closeEvent(QCloseEvent *a_event)
{
    a_event->ignore();

    if (parentWidget())
    {
        qApp->setActiveWindow(parentWidget());
        parentWidget()->setFocus();
    }

    hide();
}

void PauseWid::on_m_pbClose_clicked()
{

}

void PauseWid::on_m_lvPausas_clicked(const QModelIndex &index)
{
    Q_UNUSED(index)
}
