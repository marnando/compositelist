#ifndef SETTINGS_H
#define SETTINGS_H

#include <QWidget>

namespace Ui {
class Settings;
}

class Settings : public QWidget
{
    Q_OBJECT

public:
    explicit Settings(QWidget *parent = 0);
    ~Settings();

private slots:
    void on_m_gbRoot_clicked(bool a_checked);
    void on_m_cbIdioma_currentIndexChanged(const QString &a_arg1);
    void on_m_leMaticula_editingFinished();

private:
    Ui::Settings *ui;
};

#endif // SETTINGS_H
