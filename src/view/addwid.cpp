#include "addwid.h"
#include "ui_addwid.h"
#include "QMessageBox"

AddWid::AddWid(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddWid)
{
    ui->setupUi(this);
}

AddWid::~AddWid()
{
    delete ui;
}

bool AddWid::verifyFields()
{
    if (!ui->m_leMatricula->text().isEmpty()
    &&  !ui->m_leNome->text().isEmpty())
    {
        return true;
    } else
    {
        QMessageBox::information(this
                               , tr("Atenção")
                               , tr("Preencha corretamente os campos: MATRÍCULA e NOME.")
                               , QMessageBox::Ok);
        return false;
    }
}

void AddWid::closeEvent(QCloseEvent *a_event)
{
    a_event->ignore();

    if (parentWidget())
    {
        qApp->setActiveWindow(parentWidget());
        parentWidget()->setFocus();
    }

    hide();
}

void AddWid::on_m_pbCancela_clicked()
{
    close();

    if (parentWidget())
    {
        qApp->setActiveWindow(parentWidget());
        parentWidget()->setFocus();
    }
}

void AddWid::on_m_pbConfirma_clicked()
{
    if (verifyFields())
    {
        QString matricula = ui->m_leMatricula->text();
        QString nome = ui->m_leMatricula->text();
        QString fone = ui->m_leFone->text();
        bool isFirst = ui->m_cbPrimeiroDaLista->isChecked();

        emit signal_addNewContact(QPair<QStringList, bool>((QStringList() << matricula << nome << fone), isFirst));
    }
}
