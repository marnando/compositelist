#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>

namespace Ui {
class MainWindow;
}

class ComposeList;
class Statics;
class Settings;
class Atendance;
class PauseWid;
class AddWid;
class RemoveWid;
class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    QString m_rootWidObjectName;

    ComposeList * m_composeList;
    Statics     * m_statics;
    Settings    * m_settings;
    Atendance   * m_atendance;
    PauseWid    * m_pauseWid;
    AddWid      * m_addWid;
    RemoveWid   * m_removeWid;

    Qt::Orientations m_orientation;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void initUi();
    void centralizeMyWidget(QWidget *a_miMod
                          , double a_spacer = 1.5
                          , double a_percentMaxHeigth = 0
                          , double a_percentMaxWidth = 0);

    ComposeList *   getComposeListWid();
    Statics *       getStaticsWid();
    Settings *      getSettingesWid();
    Atendance *     getAtendanceWid();
    AddWid *        getAddWid();
    PauseWid *      getPauseWid();
    RemoveWid *     getRemoveWid();

protected:
    void resizeEvent(QResizeEvent *a_event);
    void closeEvent(QCloseEvent *a_event);

    Qt::Orientation getOrientation();

    void messageIntInfo(QString a_text, QList<int> a_vars);

signals:
    void gerarConnectsAddWid();
    void gerarConnectPauseWid();
    void gerarConnectRemoveWid();

private slots:
    void onShowComposeList();
    void onShowStaticsList();
    void onShowSettings();

    void on_m_pbAdd_clicked();
    void on_m_pbRemove_clicked();
    void on_m_pbPause_clicked();
    void on_m_pbStatistic_clicked();
    void on_m_pbBack_clicked();
    void on_m_pbSettings_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
