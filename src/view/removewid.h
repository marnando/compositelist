#ifndef REMOVEWID_H
#define REMOVEWID_H

#include <QWidget>
#include <QCloseEvent>

namespace Ui {
class RemoveWid;
}

class RemoveWid : public QWidget
{
    Q_OBJECT

public:
    explicit RemoveWid(QWidget *parent = 0);
    ~RemoveWid();

    void closeEvent(QCloseEvent * a_event);

private slots:
    void on_m_leMatricula_editingFinished();
    void on_m_pbCancelar_clicked();
    void on_m_pbConfirmar_clicked();

private:
    Ui::RemoveWid *ui;
};

#endif // REMOVEWID_H
