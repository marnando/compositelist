#ifndef ADDWID_H
#define ADDWID_H

#include <QWidget>
#include <QPair>
#include <QCloseEvent>

namespace Ui {
class AddWid;
}

class AddWid : public QWidget
{
    Q_OBJECT

public:
    explicit AddWid(QWidget *parent = 0);
    ~AddWid();

    bool verifyFields();

    void closeEvent(QCloseEvent * a_event);

signals:
    void signal_addNewContact(QPair<QStringList, bool> a_pair);

private slots:
    void on_m_pbCancela_clicked();
    void on_m_pbConfirma_clicked();


private:
    Ui::AddWid *ui;
};

#endif // ADDWID_H
