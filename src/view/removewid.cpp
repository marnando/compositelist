#include "removewid.h"
#include "ui_removewid.h"

RemoveWid::RemoveWid(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RemoveWid)
{
    ui->setupUi(this);
}

RemoveWid::~RemoveWid()
{
    delete ui;
}

void RemoveWid::closeEvent(QCloseEvent *a_event)
{
    a_event->ignore();

    if (parentWidget())
    {
        qApp->setActiveWindow(parentWidget());
        parentWidget()->setFocus();
    }

    hide();
}

void RemoveWid::on_m_leMatricula_editingFinished()
{

}

void RemoveWid::on_m_pbCancelar_clicked()
{

}

void RemoveWid::on_m_pbConfirmar_clicked()
{

}
