#ifndef STATICS_H
#define STATICS_H

#include <QWidget>

namespace Ui {
class Statics;
}

class Statics : public QWidget
{
    Q_OBJECT

public:
    explicit Statics(QWidget *parent = 0);
    ~Statics();

private slots:
    void on_m_pbRaletorio_clicked();

    void on_m_pbCalendar_clicked();

    void on_m_pbFechar_clicked();

private:
    Ui::Statics *ui;
};

#endif // STATICS_H
