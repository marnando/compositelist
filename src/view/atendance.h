#ifndef ATENDANCE_H
#define ATENDANCE_H

#include <QWidget>

namespace Ui {
class Atendance;
}

class Atendance : public QWidget
{
    Q_OBJECT

public:
    explicit Atendance(QWidget *parent = 0);
    ~Atendance();

private slots:
    void on_m_pbAtendimento_clicked();
    void on_m_pbCancelar_clicked();
    void on_m_pbConfirmar_clicked();

private:
    Ui::Atendance *ui;
};

#endif // ATENDANCE_H
