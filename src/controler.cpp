#include "controler.h"
#include "src/view/mainwindow.h"

Controler::Controler(QObject *parent) : QObject(parent)
         , m_mainWindow(new MainWindow())
{
}

Controler::~Controler()
{
    Connection::release();

    if (m_mainWindow)
    {
        delete m_mainWindow;
        m_mainWindow = NULL;
    }
}

void Controler::init()
{
    Connection::instanceDB();
    m_mainWindow->show();
}

