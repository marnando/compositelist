#ifndef CONTROLER_H
#define CONTROLER_H

#include <QObject>
#include "src/dao/connection.h"

class MainWindow;
class Controler : public QObject
{
    Q_OBJECT
public:
    MainWindow * m_mainWindow;

public:
    explicit Controler(QObject *parent = 0);
    ~Controler();

    void init();

signals:

public slots:
};

#endif // CONTROLER_H
