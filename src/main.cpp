#include "src/controler.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Controler controler;
    controler.init();

    return a.exec();
}
