#-------------------------------------------------
#
# Project created by QtCreator 2014-12-23T10:36:20
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ComposeList
TEMPLATE = app

SOURCES += src/main.cpp\
    src/controler.cpp \
    src/dao/connection.cpp \
    src/view/mainwindow.cpp \
    src/view/composelist.cpp \
    src/view/statics.cpp \
    src/view/settings.cpp \
    src/view/atendance.cpp \
    src/view/addwid.cpp \
    src/view/removewid.cpp \
    src/view/pausewid.cpp

HEADERS  += src/controler.h \
    src/dao/connection.h \
    src/view/mainwindow.h \
    src/view/composelist.h \
    src/view/statics.h \
    src/view/settings.h \
    src/view/atendance.h \
    src/view/addwid.h \
    src/view/removewid.h \
    src/view/pausewid.h

FORMS += src/forms/mainwindow.ui \
    src/forms/composelist.ui \
    src/forms/statics.ui \
    src/forms/settings.ui \
    src/forms/atendance.ui \
    src/forms/addwid.ui \
    src/forms/removewid.ui \
    src/forms/pausewid.ui

CONFIG += mobility
MOBILITY =

RESOURCES += \
    qrc/qrc.qrc

